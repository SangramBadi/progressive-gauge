import React ,{Component} from 'react';
import './App.css';
import { Table } from 'react-bootstrap';
import Form from 'react-bootstrap/Form';
import 'react-bootstrap';
import DonutChart from 'react-donut-chart';
import ReactApexChart from 'react-apexcharts';
import { cubicBezier, Gauge } from 'gauge-chart-js';

class App extends Component{
  constructor(props){
    super(props);
    this.state={
	  isLoaded:false,
	  error:"",
	  value:90,
	  openChart:false,
	  options: {
		chart: {
		  offsetY: -10
		},
		plotOptions: {
		  radialBar: {
			startAngle: -135,
			endAngle: 135,
			fill:"#fac5c7",
			dataLabels: {
				border:"1px solid #fac5c7",
			  name: {
				fontSize: '14px',
				color: "#6e6e6e",
				offsetY: 20,
			  },
			  value: {
				offsetY: -20,
				fontSize: '25px',
				color: "#6e6e6e",
				formatter: function (val) {
				  return val + "M";
				}
			  }
			}
		  }
		},
		fill: {
			opacity: 1.5,
			colors: ['#bd7800'],
			type: 'solid',
			gradient: {
				shade: '#fac5c7',
				type: "horizontal",
				shadeIntensity: 0.2,
				gradientToColors: "#fac5c7",
				inverseColors: false,
				opacityFrom: 1,
				opacityTo: 1,
				colorStops: [],
			},
		},
		stroke: {
			color:"#fac5c7"
		},
		labels: ['Audience']
	  },
	  series: [67],
	}

  }
  async componentDidMount()
  {
    await this.setState(
	{
		isLoaded:true,
        openChart:true,
	});
	if(document.getElementById("SvgjsCircle1013"))
	{
	  console.log(document.getElementById("chart"));
	  document.getElementById("SvgjsCircle1013").setAttribute("stroke","#ececec");
	  document.getElementById("SvgjsCircle1013").setAttribute("stroke-width","1");
	  document.getElementById("SvgjsCircle1013").setAttribute("r",64);
	}
  }
  componentDidUpdate()
  {
	  if(document.getElementById("SvgjsCircle1013"))
	  {
		console.log(document.getElementById("chart"));
		document.getElementById("SvgjsCircle1013").setAttribute("stroke","#ececec");
		document.getElementById("SvgjsCircle1013").setAttribute("stroke-width","1");
		document.getElementById("SvgjsCircle1013").setAttribute("r",64);
	  }
}

  changeValue = (e) =>
  {
	if(document.getElementById("SvgjsCircle1013"))
	{
	  console.log(document.getElementById("chart"));
	  document.getElementById("SvgjsCircle1013").setAttribute("stroke","#ececec");
	  document.getElementById("SvgjsCircle1013").setAttribute("stroke-width","1");
	  document.getElementById("SvgjsCircle1013").setAttribute("r",64);
	}
	console.log(document.getElementById("SvgjsCircle1013"));
	  this.setState(
	  {
		 series:[],
	  });
	  if(e.target.value > 100 ||e.target.value < 0)
	  {
		  this.setState(
		  {
			  error:"Please enter value between 1 and 100%!"
		  })
		return;
	  }
	  else if(!e.target.value)
	  {
		this.setState(
		{
			series:[90],
			error:""
		})		  
	  }
	  else if(isNaN(e.target.value))
	  {
		this.setState(
		{
			error:"Please Enter a number!",
			series:[90],
		})
	  }
	  else
	  {
		this.setState(
		{
			error:"",
			series:[e.target.value],
		})
	  }
  }
  makeCircle = (values,total) =>
  {
	  return(
		  <div>
			  <p>total</p>
		  </div>
	  )
  }
 render(){
     var {isLoaded} =this.state;
	 
	 if(!(isLoaded)){
      return(
        <div>Loading....</div>
      );  
     }
     else{
      return(
    <div className="mt-4"> 
        <div className="App ">

				<Form>
				<Form.Group controlId="formBasic">
					<Form.Label>Value</Form.Label>
					<Form.Control onChange={this.changeValue} type="text" placeholder="Enter Value (%)" />
				</Form.Group>

				<span>{this.state.error}</span>
				</Form>
				
				<div id="chart">
					<ReactApexChart options={this.state.options} series={this.state.series} type="radialBar" height="480" />
				</div>
			
				<div>
					<p className="mytext2">0</p>
					<p className="mytext">100M</p>
				</div>

        </div>
	</div>
      );
    }
  }
}

export default App;
