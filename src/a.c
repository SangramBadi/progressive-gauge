#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#define MAX 30


typedef struct Operators{ 
    char data;
    struct Operators *nextNode;
}Operators; 

typedef struct Operands{ 
    char data[MAX];
    struct Operands *nextNode;
}Operands; 
 
Operators *operators_top = NULL; 
Operands *operands_top = NULL; 

FILE *defaultPtr, *inPtr, *outPtr; 
char inMode; 

char * infix_to_postfix(char inArray[], const int last_element);
char * infix_to_prefix(char inArray[], const int last_element);

char * postfix_to_infix(char inArray[], const int last_element);
char * prefix_to_infix(char inArray[], const int last_element);

bool menu(char inArray[]);
bool isOpened(char in);
bool isClosed(char in);

void push(char in[], int code);
void pop(int code);

int main()
{
    char input[MAX]={'\0'}, location[MAX];
    int runs=1;

    while (1)
    {
        if (runs!=1) 
            system("cls"); 
        fflush(stdin); 

        printf("How would you like to input ? A - External file.\n or B - Keyboard.\n? ");
        scanf(" %c", &inMode);
        
        fflush(stdin); 
        inMode = toupper(inMode); 
        
        switch (inMode)
        {
            case 'A': 
                printf("Enter the file address ");
                scanf("%s", location);
                if (strcmp(location, "-1\0")==0) 
                    strcpy(location, "c:\\G51PGA\\default.txt");

                if ((defaultPtr = fopen(location, "a+")) == NULL)
                { 
                    puts("Could not find file\n");
                    return 0;
                }
                else 
                {
                    fgets(input, MAX, defaultPtr); 
                    printf("\nDetected input expression : %s\n\n", input); 
                }
                if (!menu(input)) 
                    return 0;
                break;

            case 'B': 
                printf("Enter the expression: "); 
                fgets(input, MAX, stdin); 
                inPtr = fopen("c:\\G51PGA\\input.txt", "w"); 
                fprintf(inPtr, "%s\n", input); 
                puts("Input was saved to c:\\G51PGA\\input.txt\n");
                fclose(inPtr);
                outPtr = fopen("c:\\G51PGA\\output.txt", "w"); 
                if (!menu(input)) 
                    return 0;
                break;
            default:
                puts("Invalid option selected.\n");
        } 
        runs++;
    } 
    return 0; 
    }

bool menu(char inArray[])
{

    char temp[MAX]={'\0'}, output[MAX]={'\0'}, exp[5]={'\0'}, str[MAX], *out=NULL, choice, conversion_instruction;
    int type=0, i, opening=0, closing=0, j, count;

    for (i=0, j=0; i<=strlen(inArray); i++)
    {
        if (inArray[i]!='\0' && inArray[i]!='(' && inArray[i]!=')')
            exp[j++]=inArray[i];
    } 
    for (i=0, j=0, count=0; i<=strlen(exp); i++)
    {
        if (isspace(exp[i]) && count==0) 
            continue;

        if (exp[i]=='+'||exp[i]=='*'||exp[i]=='/'||exp[i]=='^')
            str[j++] = 'o';

        else if (exp[i]=='-')
        { 
            if (isalnum(exp[i+1]))
            { 
                if (isdigit(exp[i+2])) 
                    i+=2; 
                else
                    i++; 
                str[j++] = 'd';
            }
            else
                str[j++] = 'o';
        }
        else 
        {
            if (isalnum(exp[i]))
                str[j++] = 'd';
            else if (isspace(exp[i]))
                str[j++] = ' ';
        } 
        count++; 
    } 
    if (str[0]=='d')
    {
        if (str[1]=='d')
        {
            if (str[2]==' ')
                type = (str[3]=='d')? 3 : 1; 
            else
                type = 1; 
        }
        else if (str[1]==' ')
            type = (str[2]=='d')? 3 : 1; 
        else
            type = 1; 
    }
    else if (str[0]=='o')
    {
        if (str[1]=='d')
            type == 1; 
        else
            type = 2; 
    } 
    if (type == 0)
    { 
        printf("\nInvalid option\n");
        return 0; 
    }

    for (i=0; inArray[i]!='\0'; i++)
    { 

        opening += (isOpened(inArray[i]))? 1 : 0;

        closing += (isClosed(inArray[i]))? 1 : 0;
    } 
        if (type==1 && opening!=closing)
        { 
            printf("Imbalanced parentheses. error!\n");
            return 0; 
        }
        else if ((type==2 || type==3) && (opening!=0 || closing!=0))
        { 
            printf("Invalid option , Postfix and Prefix expressions should be free from parenthesis. error!\n");
            return 0; 
        } 

    for (i=0; i<=strlen(inArray); i++)
    { 
        if (inArray[i]!='\0' && !isalnum(inArray[i]) && !isspace(inArray[i]))
        {
            if (!isOpened(inArray[i]) && !isClosed(inArray[i]))
            {
                if (inArray[i]!='*'&&inArray[i]!='/'&&inArray[i]!='+'&&inArray[i]!='-'&&inArray[i]!='^')
                {
                    printf("Invalid option characters. error!\n"); 
                    return 0; 
                } 
            } 
        } 
    } 


    printf("What do you want to do:\n");
    printf("(A) Convert the expression.\n(B) Evaluate the expression.?");
    scanf(" %c", &choice);
    choice = toupper(choice); 
    switch (choice)
    { 
        case 'A' : 
            switch (type)
            { 
                case 1 : 
                    printf("\nThis is an infix expression. What do you want the expression to be converted to:\n");
                    printf("(A) Postfix.\t(B) Prefix.\t(C) Both.\n? ");
                    scanf(" %c", &conversion_instruction); 
                    conversion_instruction = toupper(conversion_instruction); 
                    if (conversion_instruction == 'A')
                    { 
                        strcpy(output, infix_to_postfix(inArray, strlen(inArray)));
                        printf("\nPostfix expression: %s\n", output); 
                    }
                    else if (conversion_instruction == 'B')
                    { 
                        strcpy(output, infix_to_prefix(inArray, strlen(inArray)));
                        printf("\nPrefix expression: %s\n", output); 
                    }
                    else if (conversion_instruction == 'C')
                    {  
                        strcpy(output, infix_to_postfix(inArray, strlen(inArray))); 
                        printf("\nPostfix expression: %s\n", output); 
                        out = infix_to_prefix(inArray, strlen(inArray)); 
                        printf("Prefix expression: %s\n", out); 
                        sprintf(output + strlen(output), "\n%s", out); 
                    } 
                    if (inMode=='A')
                    {
                        fprintf(defaultPtr, "\n%s\n", output); 
                        puts("Output was saved to c:\\G51PGA\\default.txt");
                        fclose(defaultPtr);
                    }
                    
                    break; 
                case 2 : 
                    printf("\nThis is a prefix expression.\n\nWhat do you want the expression to be converted to:\n");
                    printf("(A) Infix.\t(B) Postfix.\t(C) Both.\n? ");
                    scanf(" %c", &conversion_instruction); 
                    conversion_instruction = toupper(conversion_instruction);
                    if (conversion_instruction == 'A')
                    { 
                        strcpy(output, prefix_to_infix(inArray, strlen(inArray))); 
                        printf("\nInfix expression: %s\n", output); 
                    }
                    else if (conversion_instruction == 'B')
                    { 
                        strcpy(temp, prefix_to_infix(inArray, strlen(inArray))); 
                        strcpy(output, infix_to_postfix(temp, strlen(temp)));
                        printf("\nPostfix expression: %s\n", output); 
                    }
                    else if (conversion_instruction == 'C')
                    { 
                        strcpy(temp, prefix_to_infix(inArray, strlen(inArray))); 
                        printf("\nInfix expression: %s\n", temp); 
                        out = infix_to_postfix(temp, strlen(temp)); 
                        printf("Postfix expression: %s\n", out); 
                        sprintf(output, "%s\n%s", temp, out); 
                    } 
                    if (inMode=='A')
                    { 
                        fprintf(defaultPtr, "\n%s\n", output); 
                        puts("Output was saved to c:\\G51PGA\\default.txt");
                        fclose(defaultPtr);
                    }
                    else 
                    { 
                        fprintf(outPtr, "%s\n", output); 
                        puts("Output was saved to c:\\G51PGA\\output.txt");
                        fclose(outPtr);
                    }
                
                    break; 
            } 
            break;
        case 'B': 
            for (i=0; i<=strlen(inArray); i++)
            { 
                if (isalpha(inArray[i]))
                {
                    puts("\nCannot perform evaluation.\n ! error");
                    puts("Evaluated expressions should consist of numbers and operators only!error.\n");
                    return 0;
                } 
            } 
            switch (type)
            { 
                case 1 : 
                    out = infix_to_postfix(inArray, strlen(inArray)); 
                    strcpy(temp, out); 
                    printf("\n%s\n", output); 
                    break; 
                case 2 : 
                    out = prefix_to_infix(inArray, strlen(inArray)); 
                    strcpy(temp, out); 
                    out = infix_to_postfix(temp, strlen(temp)); 
                    printf("\n%s\n", output); 
                    break; 
                case 3 : 
                    printf("\n%s\n", output); 
                    break; 
            } 
            if (inMode=='A')
            { 
                fprintf(defaultPtr, "\n%s\n", output); 
                puts("Output was saved to c:\\G51PGA\\default.txt");
                fclose(defaultPtr);
            }
            else 
            { 
                fprintf(outPtr, "%s\n", output);
                puts("Output was saved to c:\\G51PGA\\output.txt");
                fclose(outPtr);
            } 
            break; 
        case 'C' :
            return 0; 
        default :
            puts("Invalid option error.\n");
    } 

    printf("\nWhat you want to do next:\n");
    printf("(A) Enter a new expression.\t(B) End program.\n? ");
    scanf(" %c", &choice);
    if (choice == 'a' || choice == 'A')
        return 1; 
    else
    {
        return 0;  
    }
} 

char * postfix_to_infix(char inArray[], const int last_element)
{
    
    static char outArray[MAX]={'\0'};
    char operand1[MAX]={'\0'}, operand2[MAX]={'\0'}, temp[MAX]={'\0'};
    int i, operand_count=0;

    for (i=0; i<=last_element; i++)
    { 
        if (isalpha(inArray[i]))
        { 
            sprintf(temp, "%c \0", inArray[i]); 
            push(temp, 2); 
            operand_count++;
        }
        else if (isdigit(inArray[i]))
        { 
            if (isdigit(inArray[i+1]))
            {
                sprintf(temp, "%c%c \0", inArray[i], inArray[i+1]); 
                i++; 
            }
            else { 
                sprintf(temp, "%c \0", inArray[i]); 
            } 
            push(temp, 2); 
            operand_count++;
        }
        else if (inArray[i]=='+'||inArray[i]=='-'||inArray[i]=='*'||inArray[i]=='/'||inArray[i]=='^')
        { 
            if (inArray[i]=='-')
            { 
                if (isdigit(inArray[i+1]))
                {
                    if (isdigit(inArray[i+2]))
                    { 
                        sprintf(temp, "%c%c%c \0", inArray[i], inArray[i+1], inArray[i+2]); 
                        i+=2; 
                    }
                    else { 
                        sprintf(temp, "%c%c \0", inArray[i], inArray[i+1]); 
                        i++; 
                    }
                    push(temp, 2); 
                    operand_count++;
                    continue; 
                }
            } 
            if (operand_count>=2)
            { 
                strcpy(operand2, operands_top->data);
                pop(2); 
                strcpy(operand1, operands_top->data); 
                pop(2); 
                sprintf(temp, "( %s%c %s) ", operand1, inArray[i], operand2);
                push(temp, 2); 
                operand_count--;
            } 
        } 
        } 
    while (operands_top!=NULL)
    { 
        strcat(outArray, operands_top->data); 
        pop(2); 
    } 

    return outArray; 
}

char * infix_to_postfix(char inArray[], const int last_element)
{

    static char outArray[MAX]={'\0'};
    char temp[2];
    int i, j;

    for (i=0, j=0; i<=last_element; i++)
    { 
        if (isalpha(inArray[i]))
        { 
            outArray[j++]=inArray[i]; 
        }
        else if (isdigit(inArray[i]))
        { 
            if (isdigit(inArray[i+1]))
            { 
                sprintf(outArray + j, "%c%c ", inArray[i], inArray[i+1]);
                j+=3;
                i++; 
            }
            else { 
                sprintf(outArray + j, "%c ", inArray[i]);
                j+=2; 
            } 
        }
        else if (isOpened(inArray[i]))
        { 
            strcpy(temp, "(\0");
            push(temp, 1); 
        }
        else if (isClosed(inArray[i]))
        { 
            while (operators_top!=NULL && !isOpened(operators_top->data))
            {
                sprintf(outArray + j, "%c ", operators_top->data);
                pop(1); 
                j+=2;
            }
            pop(1); 
        }
        else if (inArray[i]=='+'||inArray[i]=='-'||inArray[i]=='*'||inArray[i]=='/'||inArray[i]=='^')
        { 
            if (inArray[i]=='-')
            { 
                if (isdigit(inArray[i+1]))
                { 
                    if (isdigit(inArray[i+2]))
                    {
                        sprintf(outArray + j, "%c%c%c ", inArray[i], inArray[i+1], inArray[i+2]);
                        j+=4;
                        i+=2; 
                    }
                    else {
                        sprintf(outArray + j, "%c%c ", inArray[i], inArray[i+1]);
                        j+=3;
                        i++; 
                    } 
                    continue; 
                } 
            } 
            while (operators_top!=NULL &&  !isOpened(operators_top->data))
            {
                
                sprintf(outArray + j, "%c ", operators_top->data); 
                pop(1); 
                j+=2;
            } 
            temp[0] = inArray[i]; 
            temp[1] = '\0';
            push(temp, 1); 
        } 
    } 

    while (operators_top!=NULL)
    {  
        sprintf(outArray + j, "%c ", operators_top->data);
        pop(1);
    } 

    return outArray;
} 

char * infix_to_prefix(char inArray[], const int last_element)
{

    static char outArray[MAX]={'\0'};
    char tempArray[MAX]={'\0'}, temp[2];
    int i, m, n, j;

    strrev(inArray); 

    for (i=0, j=0; i<=last_element; i++)
    { 
        if (isalpha(inArray[i]))
        { 
            outArray[j++]=inArray[i]; 
        }
        else if (isdigit(inArray[i]))
        { 
            if (isdigit(inArray[i+1]))
            { 
                if (inArray[i+2]=='-')
                { 
                    sprintf(outArray + j, "%c%c%c ", inArray[i], inArray[i+1], inArray[i+2]);
                    j+=4; 
                    i+=2; 
                    continue; 
                } 
                
                sprintf(outArray + j, "%c%c ", inArray[i], inArray[i+1]);
                j+=3; 
                i++;
            }
            else { 
                sprintf(outArray + j, "%c ", inArray[i]);
                j+=2; 
            } 
        }
        else if (isClosed(inArray[i]))
        { 
            strcpy(temp, ")\0"); 
            push(temp, 1);
        }
        else if (isOpened(inArray[i]))
        { 
            while (operators_top!=NULL && !isClosed(operators_top->data))
            { 
                sprintf(outArray + j, "%c ", operators_top->data); 
                pop(1); 
                j+=2; 
            } 
            pop(1); 
        }
        else if (inArray[i]=='+'||inArray[i]=='-'||inArray[i]=='*'||inArray[i]=='/'||inArray[i]=='^')
        {
            while (operators_top!=NULL && !isClosed(operators_top->data))
            {
                
                sprintf(outArray + j, "%c ", operators_top->data);
                pop(1); 
                j+=2; 
            }
            temp[0]=inArray[i];
            temp[1]='\0';
            push(temp, 1); 
        } 
    } 

    while (operators_top!=NULL)
    { 
        sprintf(outArray + j, "%c ", operators_top->data);
        pop(1);
        j+=2; 
    } 

    return strrev(outArray); 
} 

void pop(int code)
{
    
    Operators *tempOperatorPtr;
    Operands *tempOperandPtr;

    switch (code)
    {
        case 1: 
            if (operators_top->nextNode == NULL) 
                operators_top = NULL; 
            else { 
                tempOperatorPtr = operators_top; 
                operators_top = operators_top->nextNode; 
                free(tempOperatorPtr);
            } 
            break;
        case 2: 
            if (operands_top != NULL)
            { 
                tempOperandPtr = operands_top; 
                operands_top = tempOperandPtr->nextNode; 
                free(tempOperandPtr);
            } 
            break;
       
    } 
}

int priority(char in)
{ 
    switch(in)
    {
        case '(':
            return 1;

        case '+':
        case '-':
            return 2;

        case '*':
        case '/':
            return 3;

        case '^':
            return 4;

    } 
} 

bool isOpened(char in)
{ 
    return (in=='('||in=='['||in=='{')? 1 : 0;
}

bool isClosed(char in)
{ 
    
    return (in==')'||in==']'||in=='}')? 1 : 0;
}

char * prefix_to_infix(char inArray[], const int last_element)
{
    
    static char outArray[MAX]={'\0'};
    char operand1[MAX]={'\0'}, operand2[MAX]={'\0'}, temp[MAX]={'\0'};
    int i, operand_count=0;

    strrev(inArray); 

    for (i=0; i<=last_element; i++)
    { 
        if (isalpha(inArray[i]))
        { 
            sprintf(temp, "%c \0", inArray[i]); 
            push(temp, 2); 
            operand_count++;
        }
        else if (isdigit(inArray[i]))
        { 
            if (isdigit(inArray[i+1]))
            { 
                if (inArray[i+2]=='-')
                { 
                    sprintf(temp, "%c%c%c \0", inArray[i+2], inArray[i+1], inArray[i]); 
                    i+=2; 
                }
                else { 
                    sprintf(temp, "%c%c \0", inArray[i+1], inArray[i]);
                    i++;
                } 
            }
            else { 
                sprintf(temp, "%c \0", inArray[i]); 
            } 
            push(temp, 2); 
            operand_count++;
        }
        else if (inArray[i]=='+'||inArray[i]=='-'||inArray[i]=='*'||inArray[i]=='/'||inArray[i]=='^')
        {
            if (operand_count>=2)
            { 
                strcpy(operand2, operands_top->data); 
                pop(2); 
                strcpy(operand1, operands_top->data); 
                pop(2); 
                
                sprintf(temp, "( %s%c %s) ", operand2, inArray[i], operand1);
                push(temp, 2); 
                operand_count--;
            } 
        } 
    } 
    while (operands_top!=NULL)
    { 
        strcat(outArray, operands_top->data); 
        pop(2);
    } 

    return outArray; 
} 


void push(char in[], int code)
{

    Operators *tempOperatorPtr;
    Operands *tempOperandPtr;
    int int_val, i, j;
    char exp[5]={'\0'};

    switch (code)
    { 
        case 1: 
            tempOperatorPtr=(Operators *)malloc(sizeof(Operators)); 
            tempOperatorPtr->data = in[0]; 
            if (operators_top==NULL) 
                tempOperatorPtr->nextNode = NULL; 
            else 
                tempOperatorPtr->nextNode = operators_top; 
            operators_top = tempOperatorPtr; 
            break;

        case 2: 
            tempOperandPtr=(Operands *)malloc(sizeof(Operands)); 
            strcpy(tempOperandPtr->data, in); 
            if (operands_top==NULL) 
                tempOperandPtr->nextNode = NULL; 
            else 
                tempOperandPtr->nextNode = operands_top; 
            operands_top = tempOperandPtr; 
            break;
        case 3:
            for (i=0, j=0; i<=strlen(in); i++)
            {
                if (isdigit(in[i]) || in[i]=='-')
                    exp[j++] = in[i]; 
            } 
            int_val = atoi(exp);
           
            break;
    } 
} 

